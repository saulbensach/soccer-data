import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { ApiService } from './api.service';
import { Match } from './match';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, AfterViewInit {
  title = 'soccer-data';
  displayedColumns: string[] = ['Date', 'AwayTeam', 'HomeTeam', 'FTHG', 'FTAG', 'HC', 'AC'];
  dataSource = new  MatTableDataSource<Match> ();

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private api: ApiService) {}

  ngOnInit() {
    this.api.getMatches().subscribe(data => {
        this.dataSource.data = data;
        this.paginator.nextPage;
    })
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

}
