import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {Match} from './match';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  public getMatches(): Observable<Match[]>{
    return this.http.get<Match[]>("assets/sports-data/spanish-la-liga/data/season-1718_json.json")
  }

}
