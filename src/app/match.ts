export class Match {
    AC: number;
    AF: number;
    AR: number;
    AS: number;
    AST: number;
    AY: number;
    AwayTeam: string;
    Date: string;
    FTAG: number;
    FTHG: number;
    FTR: number;
    HC: number;
    HF: number;
    HR: number;
    HS: number;
    HST: number;
    HTAG: number;
    HTHG: number;
    HTR: string;
    HY: number;
    HomeTeam: string;
}